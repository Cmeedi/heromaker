﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroApp
{
    public class SuperHeroList
    {

        public List<SuperHero> ListofHeroes { get; set; }

        public SuperHeroList()
        {

            ListofHeroes = new List<SuperHero>();

        }
    }
}
