﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroApp
{
    public partial class Form2 : Form
    {


        public SuperHeroList formOneHeroList;
        SuperHeroList sList = new SuperHeroList();
        Form1 f1;


        public Form2(SuperHeroList mailed, Form1 fo1)
        {
            InitializeComponent();
            formOneHeroList = mailed;
            f1 = fo1;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
            SuperHeroListBox.Items.Clear();
            foreach(var Hero in formOneHeroList.ListofHeroes)
            {
                SuperHeroListBox.Items.Add(Hero.Name);
            }
            SuperHeroListBox.SelectedIndex = 0;

        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            f1.Show();
        }

        private string Info(string name)
        {

           var something = formOneHeroList.ListofHeroes.Find(n => n.Name == name);
            if(something.Element == "Fire")
            {
                BackgroundImage = HeroApp.Properties.Resources.Fire;
            }
            else if(something.Element == "Ice")
            {
                BackgroundImage = HeroApp.Properties.Resources.Ice;
            }
            else
            {
                BackgroundImage = HeroApp.Properties.Resources.Tyreal;
            }

            return something.ToString();
        }

        private void SuperHeroListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbDescription.Text = Info(SuperHeroListBox.SelectedItem.ToString());
            


        }
    }
}
