﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeroApp
{

    public partial class Form1 : Form
    {

        public SuperHeroList mySuperHeroList = new SuperHeroList();


        public Form1()
        {

            InitializeComponent();

        }

        public void GetData(SuperHero superHeros)
        {
            try
            {

                SuperHero sh = new SuperHero(HeroNameTextBox.Text);
                superHeros.name = sh.Name;
                superHeros.strength = StrengthhScrollBar.Value;
                superHeros.dexterity = DexterityhScrollBar.Value;
                superHeros.magic = MagichScrollBar.Value;
                superHeros.race = RaceListBox.SelectedItem.ToString();
                superHeros.darkness = DarkPolarityTrackBar.Value;

                if (FireRadioButton.Checked)
                {
                    superHeros.element = "Fire";
                }
                else
                {
                    superHeros.element = "Ice";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        public void CreateHero(SuperHero superHeros)
        {

            GetData(superHeros);

        }

        public void display()
        {



        }

        //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //These are some functions within the form that don't rely on the create button to be pressed but 
        //are implemented immediatly after certain form controls are altered



        //This method allows the program to not only switch between the two images saved into the resx folder
        //but it also allows the program to switch back and forth between the fire and ice group boxes
        private void FireRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (FireRadioButton.Checked)
            {
                FireGroupBox.Visible = true;
                IceGroupBox.Visible = false;
                BackgroundImage = HeroApp.Properties.Resources.Fire;
            }
            if (IceRadioButton.Checked)
            {
                IceGroupBox.Visible = true;
                FireGroupBox.Visible = false;
                BackgroundImage = HeroApp.Properties.Resources.Ice;
            }
        }

        //This converts the Strength Hscroll to text and displays it onto a label above the scroll bar
        private void StrengthhScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            StrengthLabel.Text = "Strength: " + StrengthhScrollBar.Value.ToString();
        }

        //This converts the Dexterity Hscroll to text and displays it onto a label above the scroll bar
        private void DexterityhScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            DexterityLabel.Text = "Dexterity: " + DexterityhScrollBar.Value.ToString();
        }

        //This converts the Magic Hscroll to text and displays it onto a label above the scroll bar
        private void MagichScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            MagicLabel.Text = "Magic: " + MagichScrollBar.Value.ToString();
        }

        private void RacePictureBox_Click(object sender, EventArgs e)
        {
        }

        //This method attaches the RaceListBox selected item with the picture being displayed onto the
        //picture box by using the selected index and assigning it to its corresponding picture within the 
        // .resx folder
        private void RaceListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RaceListBox.SelectedIndex == 0)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Aliens;
            }
            if (RaceListBox.SelectedIndex == 1)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Cat;
            }
            if (RaceListBox.SelectedIndex == 2)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Dark_Elf;
            }
            if (RaceListBox.SelectedIndex == 3)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Goblin;
            }
            if (RaceListBox.SelectedIndex == 4)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.High_Elf;
            }
            if (RaceListBox.SelectedIndex == 5)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Human;
            }
            if (RaceListBox.SelectedIndex == 6)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Lizard;
            }
            if (RaceListBox.SelectedIndex == 7)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Orc;
            }
            if (RaceListBox.SelectedIndex == 8)
            {
                RacePictureBox.Image = HeroApp.Properties.Resources.Wood_Elf;
            }
        }

        //Whatever number the user selects on the slider
        private void DarkPolarityTrackBar_Scroll(object sender, EventArgs e)
        {
            //will be displayed on the label above the slider
            DarkPolarityLabel.Text = "Dark Polarity: " + DarkPolarityTrackBar.Value.ToString();
        }

        private void CreateHeroButton_Click(object sender, EventArgs e)
        {

            SuperHero superHero = new SuperHero(HeroNameTextBox.Text);

            GetData(superHero);
            mySuperHeroList.ListofHeroes.Add(superHero);

            Form2 f2 = new Form2(mySuperHeroList, this);


            //This is an object of the sum of all the attributes
            int sum = StrengthhScrollBar.Value + DexterityhScrollBar.Value + MagichScrollBar.Value;
            //if the attributes total exceed 100
            if (sum > 100)
            {
                // this message will be displayed
                MessageBox.Show("The Strength, Dexterity and Magic total cannot exceed 100");
            }
            else
            {
                //This creates an object of form 2

                //Form1 becomes hidden
                this.Hide();
                //and Form2 is shown
                f2.Show();
            }

        }

        //This method allows the user to choose a specific color upon clicking the CapeColorPictureBox
        //and then displays the selected color onto the picture box
        private void CapeColorPictureBox_Click(object sender, EventArgs e)
        {
            //This is how the Color Selector is opened
            colorDialog1.ShowDialog();
            //This assigns the selected color to the picture box
            CapeColorPictureBox.BackColor = colorDialog1.Color;

        }

    }
}


