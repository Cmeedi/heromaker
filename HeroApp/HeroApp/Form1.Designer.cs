﻿namespace HeroApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeroNameLabel = new System.Windows.Forms.Label();
            this.HeroNameTextBox = new System.Windows.Forms.TextBox();
            this.FireBallCheckBox = new System.Windows.Forms.CheckBox();
            this.ImpCheckBox = new System.Windows.Forms.CheckBox();
            this.MeteorCheckBox = new System.Windows.Forms.CheckBox();
            this.FireGroupBox = new System.Windows.Forms.GroupBox();
            this.FireRadioButton = new System.Windows.Forms.RadioButton();
            this.IceRadioButton = new System.Windows.Forms.RadioButton();
            this.IceGroupBox = new System.Windows.Forms.GroupBox();
            this.FrozenArmorCheckBox = new System.Windows.Forms.CheckBox();
            this.HailCheckBox = new System.Windows.Forms.CheckBox();
            this.IceSwordCheckBox = new System.Windows.Forms.CheckBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.DayOfReckoningLabel = new System.Windows.Forms.Label();
            this.RacePictureBox = new System.Windows.Forms.PictureBox();
            this.RaceListBox = new System.Windows.Forms.ListBox();
            this.RaceLabel = new System.Windows.Forms.Label();
            this.StrengthhScrollBar = new System.Windows.Forms.HScrollBar();
            this.StrengthLabel = new System.Windows.Forms.Label();
            this.DexterityLabel = new System.Windows.Forms.Label();
            this.DexterityhScrollBar = new System.Windows.Forms.HScrollBar();
            this.MagicLabel = new System.Windows.Forms.Label();
            this.MagichScrollBar = new System.Windows.Forms.HScrollBar();
            this.GroupSizeNumberSpinner = new System.Windows.Forms.NumericUpDown();
            this.GroupSizeLabel = new System.Windows.Forms.Label();
            this.CreateHeroButton = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.DarkPolarityTrackBar = new System.Windows.Forms.TrackBar();
            this.DarkPolarityLabel = new System.Windows.Forms.Label();
            this.CapeColorPictureBox = new System.Windows.Forms.PictureBox();
            this.CapeColorLabel = new System.Windows.Forms.Label();
            this.FireGroupBox.SuspendLayout();
            this.IceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RacePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupSizeNumberSpinner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkPolarityTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CapeColorPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // HeroNameLabel
            // 
            this.HeroNameLabel.AutoSize = true;
            this.HeroNameLabel.Location = new System.Drawing.Point(406, 167);
            this.HeroNameLabel.Name = "HeroNameLabel";
            this.HeroNameLabel.Size = new System.Drawing.Size(80, 17);
            this.HeroNameLabel.TabIndex = 0;
            this.HeroNameLabel.Text = "Hero Name";
            // 
            // HeroNameTextBox
            // 
            this.HeroNameTextBox.Location = new System.Drawing.Point(395, 187);
            this.HeroNameTextBox.Name = "HeroNameTextBox";
            this.HeroNameTextBox.Size = new System.Drawing.Size(100, 22);
            this.HeroNameTextBox.TabIndex = 1;
            this.HeroNameTextBox.Text = "Enter Name";
            // 
            // FireBallCheckBox
            // 
            this.FireBallCheckBox.AutoSize = true;
            this.FireBallCheckBox.Location = new System.Drawing.Point(6, 21);
            this.FireBallCheckBox.Name = "FireBallCheckBox";
            this.FireBallCheckBox.Size = new System.Drawing.Size(77, 21);
            this.FireBallCheckBox.TabIndex = 2;
            this.FireBallCheckBox.Text = "FireBall";
            this.FireBallCheckBox.UseVisualStyleBackColor = true;
            // 
            // ImpCheckBox
            // 
            this.ImpCheckBox.AutoSize = true;
            this.ImpCheckBox.Location = new System.Drawing.Point(22, 48);
            this.ImpCheckBox.Name = "ImpCheckBox";
            this.ImpCheckBox.Size = new System.Drawing.Size(52, 21);
            this.ImpCheckBox.TabIndex = 3;
            this.ImpCheckBox.Text = "Imp";
            this.ImpCheckBox.UseVisualStyleBackColor = true;
            // 
            // MeteorCheckBox
            // 
            this.MeteorCheckBox.AutoSize = true;
            this.MeteorCheckBox.Location = new System.Drawing.Point(38, 75);
            this.MeteorCheckBox.Name = "MeteorCheckBox";
            this.MeteorCheckBox.Size = new System.Drawing.Size(74, 21);
            this.MeteorCheckBox.TabIndex = 4;
            this.MeteorCheckBox.Text = "Meteor";
            this.MeteorCheckBox.UseVisualStyleBackColor = true;
            // 
            // FireGroupBox
            // 
            this.FireGroupBox.Controls.Add(this.FireBallCheckBox);
            this.FireGroupBox.Controls.Add(this.MeteorCheckBox);
            this.FireGroupBox.Controls.Add(this.ImpCheckBox);
            this.FireGroupBox.Location = new System.Drawing.Point(637, 149);
            this.FireGroupBox.Name = "FireGroupBox";
            this.FireGroupBox.Size = new System.Drawing.Size(110, 100);
            this.FireGroupBox.TabIndex = 5;
            this.FireGroupBox.TabStop = false;
            this.FireGroupBox.Text = "Fire";
            // 
            // FireRadioButton
            // 
            this.FireRadioButton.AutoSize = true;
            this.FireRadioButton.Location = new System.Drawing.Point(637, 122);
            this.FireRadioButton.Name = "FireRadioButton";
            this.FireRadioButton.Size = new System.Drawing.Size(53, 21);
            this.FireRadioButton.TabIndex = 6;
            this.FireRadioButton.TabStop = true;
            this.FireRadioButton.Text = "Fire";
            this.FireRadioButton.UseVisualStyleBackColor = true;
            this.FireRadioButton.CheckedChanged += new System.EventHandler(this.FireRadioButton_CheckedChanged);
            // 
            // IceRadioButton
            // 
            this.IceRadioButton.AutoSize = true;
            this.IceRadioButton.Location = new System.Drawing.Point(696, 122);
            this.IceRadioButton.Name = "IceRadioButton";
            this.IceRadioButton.Size = new System.Drawing.Size(47, 21);
            this.IceRadioButton.TabIndex = 8;
            this.IceRadioButton.TabStop = true;
            this.IceRadioButton.Text = "Ice";
            this.IceRadioButton.UseVisualStyleBackColor = true;
            // 
            // IceGroupBox
            // 
            this.IceGroupBox.Controls.Add(this.FrozenArmorCheckBox);
            this.IceGroupBox.Controls.Add(this.HailCheckBox);
            this.IceGroupBox.Controls.Add(this.IceSwordCheckBox);
            this.IceGroupBox.Location = new System.Drawing.Point(637, 149);
            this.IceGroupBox.Name = "IceGroupBox";
            this.IceGroupBox.Size = new System.Drawing.Size(110, 100);
            this.IceGroupBox.TabIndex = 7;
            this.IceGroupBox.TabStop = false;
            this.IceGroupBox.Text = "Ice";
            // 
            // FrozenArmorCheckBox
            // 
            this.FrozenArmorCheckBox.AutoSize = true;
            this.FrozenArmorCheckBox.Location = new System.Drawing.Point(6, 21);
            this.FrozenArmorCheckBox.Name = "FrozenArmorCheckBox";
            this.FrozenArmorCheckBox.Size = new System.Drawing.Size(97, 21);
            this.FrozenArmorCheckBox.TabIndex = 2;
            this.FrozenArmorCheckBox.Text = "Frost Shell";
            this.FrozenArmorCheckBox.UseVisualStyleBackColor = true;
            // 
            // HailCheckBox
            // 
            this.HailCheckBox.AutoSize = true;
            this.HailCheckBox.Location = new System.Drawing.Point(38, 75);
            this.HailCheckBox.Name = "HailCheckBox";
            this.HailCheckBox.Size = new System.Drawing.Size(54, 21);
            this.HailCheckBox.TabIndex = 4;
            this.HailCheckBox.Text = "Hail";
            this.HailCheckBox.UseVisualStyleBackColor = true;
            // 
            // IceSwordCheckBox
            // 
            this.IceSwordCheckBox.AutoSize = true;
            this.IceSwordCheckBox.Location = new System.Drawing.Point(22, 48);
            this.IceSwordCheckBox.Name = "IceSwordCheckBox";
            this.IceSwordCheckBox.Size = new System.Drawing.Size(91, 21);
            this.IceSwordCheckBox.TabIndex = 3;
            this.IceSwordCheckBox.Text = "Ice Sword";
            this.IceSwordCheckBox.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(395, 397);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // DayOfReckoningLabel
            // 
            this.DayOfReckoningLabel.AutoSize = true;
            this.DayOfReckoningLabel.Location = new System.Drawing.Point(432, 377);
            this.DayOfReckoningLabel.Name = "DayOfReckoningLabel";
            this.DayOfReckoningLabel.Size = new System.Drawing.Size(120, 17);
            this.DayOfReckoningLabel.TabIndex = 10;
            this.DayOfReckoningLabel.Text = "Day of Reckoning";
            // 
            // RacePictureBox
            // 
            this.RacePictureBox.Image = global::HeroApp.Properties.Resources.Tyreal;
            this.RacePictureBox.InitialImage = global::HeroApp.Properties.Resources.Tyreal;
            this.RacePictureBox.Location = new System.Drawing.Point(395, 215);
            this.RacePictureBox.Name = "RacePictureBox";
            this.RacePictureBox.Size = new System.Drawing.Size(200, 160);
            this.RacePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RacePictureBox.TabIndex = 11;
            this.RacePictureBox.TabStop = false;
            this.RacePictureBox.Click += new System.EventHandler(this.RacePictureBox_Click);
            // 
            // RaceListBox
            // 
            this.RaceListBox.FormattingEnabled = true;
            this.RaceListBox.ItemHeight = 16;
            this.RaceListBox.Items.AddRange(new object[] {
            "Alien",
            "Cat",
            "Dark Elf",
            "Goblin",
            "High Elf",
            "Human",
            "Lizard",
            "Orc",
            "Wood Elf"});
            this.RaceListBox.Location = new System.Drawing.Point(257, 146);
            this.RaceListBox.Name = "RaceListBox";
            this.RaceListBox.Size = new System.Drawing.Size(132, 100);
            this.RaceListBox.Sorted = true;
            this.RaceListBox.TabIndex = 12;
            this.RaceListBox.SelectedIndexChanged += new System.EventHandler(this.RaceListBox_SelectedIndexChanged);
            // 
            // RaceLabel
            // 
            this.RaceLabel.AutoSize = true;
            this.RaceLabel.Location = new System.Drawing.Point(298, 126);
            this.RaceLabel.Name = "RaceLabel";
            this.RaceLabel.Size = new System.Drawing.Size(41, 17);
            this.RaceLabel.TabIndex = 13;
            this.RaceLabel.Text = "Race";
            // 
            // StrengthhScrollBar
            // 
            this.StrengthhScrollBar.Location = new System.Drawing.Point(637, 269);
            this.StrengthhScrollBar.Name = "StrengthhScrollBar";
            this.StrengthhScrollBar.Size = new System.Drawing.Size(113, 21);
            this.StrengthhScrollBar.TabIndex = 14;
            this.StrengthhScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.StrengthhScrollBar_Scroll);
            // 
            // StrengthLabel
            // 
            this.StrengthLabel.Location = new System.Drawing.Point(640, 252);
            this.StrengthLabel.Name = "StrengthLabel";
            this.StrengthLabel.Size = new System.Drawing.Size(89, 17);
            this.StrengthLabel.TabIndex = 15;
            // 
            // DexterityLabel
            // 
            this.DexterityLabel.Location = new System.Drawing.Point(640, 290);
            this.DexterityLabel.Name = "DexterityLabel";
            this.DexterityLabel.Size = new System.Drawing.Size(89, 17);
            this.DexterityLabel.TabIndex = 17;
            // 
            // DexterityhScrollBar
            // 
            this.DexterityhScrollBar.Location = new System.Drawing.Point(637, 307);
            this.DexterityhScrollBar.Name = "DexterityhScrollBar";
            this.DexterityhScrollBar.Size = new System.Drawing.Size(113, 21);
            this.DexterityhScrollBar.TabIndex = 16;
            this.DexterityhScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.DexterityhScrollBar_Scroll);
            // 
            // MagicLabel
            // 
            this.MagicLabel.Location = new System.Drawing.Point(640, 328);
            this.MagicLabel.Name = "MagicLabel";
            this.MagicLabel.Size = new System.Drawing.Size(89, 17);
            this.MagicLabel.TabIndex = 19;
            // 
            // MagichScrollBar
            // 
            this.MagichScrollBar.Location = new System.Drawing.Point(637, 345);
            this.MagichScrollBar.Name = "MagichScrollBar";
            this.MagichScrollBar.Size = new System.Drawing.Size(113, 21);
            this.MagichScrollBar.TabIndex = 18;
            this.MagichScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.MagichScrollBar_Scroll);
            // 
            // GroupSizeNumberSpinner
            // 
            this.GroupSizeNumberSpinner.Location = new System.Drawing.Point(257, 275);
            this.GroupSizeNumberSpinner.Name = "GroupSizeNumberSpinner";
            this.GroupSizeNumberSpinner.Size = new System.Drawing.Size(120, 22);
            this.GroupSizeNumberSpinner.TabIndex = 20;
            // 
            // GroupSizeLabel
            // 
            this.GroupSizeLabel.AutoSize = true;
            this.GroupSizeLabel.Location = new System.Drawing.Point(257, 255);
            this.GroupSizeLabel.Name = "GroupSizeLabel";
            this.GroupSizeLabel.Size = new System.Drawing.Size(75, 17);
            this.GroupSizeLabel.TabIndex = 21;
            this.GroupSizeLabel.Text = "GroupSize";
            // 
            // CreateHeroButton
            // 
            this.CreateHeroButton.Location = new System.Drawing.Point(650, 387);
            this.CreateHeroButton.Name = "CreateHeroButton";
            this.CreateHeroButton.Size = new System.Drawing.Size(79, 32);
            this.CreateHeroButton.TabIndex = 22;
            this.CreateHeroButton.Text = "Create";
            this.CreateHeroButton.UseVisualStyleBackColor = true;
            this.CreateHeroButton.Click += new System.EventHandler(this.CreateHeroButton_Click);
            // 
            // DarkPolarityTrackBar
            // 
            this.DarkPolarityTrackBar.Location = new System.Drawing.Point(260, 345);
            this.DarkPolarityTrackBar.Name = "DarkPolarityTrackBar";
            this.DarkPolarityTrackBar.Size = new System.Drawing.Size(117, 56);
            this.DarkPolarityTrackBar.TabIndex = 23;
            this.DarkPolarityTrackBar.Scroll += new System.EventHandler(this.DarkPolarityTrackBar_Scroll);
            // 
            // DarkPolarityLabel
            // 
            this.DarkPolarityLabel.Location = new System.Drawing.Point(260, 322);
            this.DarkPolarityLabel.Name = "DarkPolarityLabel";
            this.DarkPolarityLabel.Size = new System.Drawing.Size(117, 20);
            this.DarkPolarityLabel.TabIndex = 24;
            // 
            // CapeColorPictureBox
            // 
            this.CapeColorPictureBox.Location = new System.Drawing.Point(501, 187);
            this.CapeColorPictureBox.Name = "CapeColorPictureBox";
            this.CapeColorPictureBox.Size = new System.Drawing.Size(94, 22);
            this.CapeColorPictureBox.TabIndex = 25;
            this.CapeColorPictureBox.TabStop = false;
            this.CapeColorPictureBox.Click += new System.EventHandler(this.CapeColorPictureBox_Click);
            // 
            // CapeColorLabel
            // 
            this.CapeColorLabel.AutoSize = true;
            this.CapeColorLabel.Location = new System.Drawing.Point(498, 167);
            this.CapeColorLabel.Name = "CapeColorLabel";
            this.CapeColorLabel.Size = new System.Drawing.Size(78, 17);
            this.CapeColorLabel.TabIndex = 26;
            this.CapeColorLabel.Text = "Cape Color";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(980, 631);
            this.Controls.Add(this.CapeColorLabel);
            this.Controls.Add(this.CapeColorPictureBox);
            this.Controls.Add(this.DarkPolarityLabel);
            this.Controls.Add(this.DarkPolarityTrackBar);
            this.Controls.Add(this.CreateHeroButton);
            this.Controls.Add(this.GroupSizeLabel);
            this.Controls.Add(this.GroupSizeNumberSpinner);
            this.Controls.Add(this.MagicLabel);
            this.Controls.Add(this.MagichScrollBar);
            this.Controls.Add(this.DexterityLabel);
            this.Controls.Add(this.DexterityhScrollBar);
            this.Controls.Add(this.StrengthLabel);
            this.Controls.Add(this.StrengthhScrollBar);
            this.Controls.Add(this.RaceLabel);
            this.Controls.Add(this.RaceListBox);
            this.Controls.Add(this.RacePictureBox);
            this.Controls.Add(this.DayOfReckoningLabel);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.IceRadioButton);
            this.Controls.Add(this.IceGroupBox);
            this.Controls.Add(this.FireRadioButton);
            this.Controls.Add(this.FireGroupBox);
            this.Controls.Add(this.HeroNameTextBox);
            this.Controls.Add(this.HeroNameLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FireGroupBox.ResumeLayout(false);
            this.FireGroupBox.PerformLayout();
            this.IceGroupBox.ResumeLayout(false);
            this.IceGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RacePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupSizeNumberSpinner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkPolarityTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CapeColorPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeroNameLabel;
        private System.Windows.Forms.CheckBox FireBallCheckBox;
        private System.Windows.Forms.CheckBox ImpCheckBox;
        private System.Windows.Forms.CheckBox MeteorCheckBox;
        private System.Windows.Forms.GroupBox FireGroupBox;
        private System.Windows.Forms.RadioButton FireRadioButton;
        private System.Windows.Forms.RadioButton IceRadioButton;
        private System.Windows.Forms.GroupBox IceGroupBox;
        private System.Windows.Forms.CheckBox FrozenArmorCheckBox;
        private System.Windows.Forms.CheckBox HailCheckBox;
        private System.Windows.Forms.CheckBox IceSwordCheckBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label DayOfReckoningLabel;
        private System.Windows.Forms.PictureBox RacePictureBox;
        private System.Windows.Forms.ListBox RaceListBox;
        private System.Windows.Forms.Label RaceLabel;
        private System.Windows.Forms.HScrollBar StrengthhScrollBar;
        private System.Windows.Forms.Label StrengthLabel;
        private System.Windows.Forms.Label DexterityLabel;
        private System.Windows.Forms.HScrollBar DexterityhScrollBar;
        private System.Windows.Forms.Label MagicLabel;
        private System.Windows.Forms.HScrollBar MagichScrollBar;
        private System.Windows.Forms.NumericUpDown GroupSizeNumberSpinner;
        private System.Windows.Forms.Label GroupSizeLabel;
        private System.Windows.Forms.Button CreateHeroButton;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TrackBar DarkPolarityTrackBar;
        private System.Windows.Forms.Label DarkPolarityLabel;
        private System.Windows.Forms.PictureBox CapeColorPictureBox;
        private System.Windows.Forms.Label CapeColorLabel;
        public System.Windows.Forms.TextBox HeroNameTextBox;
    }
}

