﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroApp
{
    public class SuperHero
    {

        public string name;
        public int strength;
        public int dexterity;
        public int magic;
        public string race;
        public string element;
        public int darkness;

        public SuperHero(string name)
        {
            this.name = name;
            this.strength = 0;
            this.dexterity = 0;
            this.magic = 0;
            this.race = " ";
            this.element = " ";
            this.darkness = 0;
        }
        
        public string Name { get => name; set => name = value; }
        public int Strength { get => strength; set => strength = value; }
        public int Dexterity { get => dexterity; set => dexterity = value; }
        public int Magic { get => magic; set => magic = value; }
        public string Race { get => race; set => race = value; }
        public string Element { get => element; set => element = value; }
        public int Darkness { get => darkness; set => darkness = value; }

        public override string ToString()
        {
            return Name + " " + Strength + " " + Dexterity + " " + Magic + " " + Race + " " +  " " + Element + " " + Darkness;
        }

    }
}
